﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XiaoYou.SelectAccountManager.WebHost.Migrations
{
    public partial class accountv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Desc",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ServeName",
                table: "Accounts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Desc",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "ServeName",
                table: "Accounts");
        }
    }
}
