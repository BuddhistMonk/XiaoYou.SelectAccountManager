﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XiaoYou.SelectAccountManager.WebHost.Migrations
{
    public partial class account_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_TagAccounts_TagAccountId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Tags_TagAccounts_TagAccountId",
                table: "Tags");

            migrationBuilder.DropForeignKey(
                name: "FK_Tags_TagTypes_TagTypeId",
                table: "Tags");

            migrationBuilder.DropIndex(
                name: "IX_Tags_TagAccountId",
                table: "Tags");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_TagAccountId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "TagAccountId",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "TagId",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "TagAccountId",
                table: "Accounts");

            migrationBuilder.AlterColumn<int>(
                name: "TagTypeId",
                table: "Tags",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TagAccounts_AccountId",
                table: "TagAccounts",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_TagAccounts_TagId",
                table: "TagAccounts",
                column: "TagId");

            migrationBuilder.AddForeignKey(
                name: "FK_TagAccounts_Accounts_AccountId",
                table: "TagAccounts",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TagAccounts_Tags_TagId",
                table: "TagAccounts",
                column: "TagId",
                principalTable: "Tags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tags_TagTypes_TagTypeId",
                table: "Tags",
                column: "TagTypeId",
                principalTable: "TagTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TagAccounts_Accounts_AccountId",
                table: "TagAccounts");

            migrationBuilder.DropForeignKey(
                name: "FK_TagAccounts_Tags_TagId",
                table: "TagAccounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Tags_TagTypes_TagTypeId",
                table: "Tags");

            migrationBuilder.DropIndex(
                name: "IX_TagAccounts_AccountId",
                table: "TagAccounts");

            migrationBuilder.DropIndex(
                name: "IX_TagAccounts_TagId",
                table: "TagAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "TagTypeId",
                table: "Tags",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "TagAccountId",
                table: "Tags",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TagId",
                table: "Tags",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TagAccountId",
                table: "Accounts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tags_TagAccountId",
                table: "Tags",
                column: "TagAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_TagAccountId",
                table: "Accounts",
                column: "TagAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_TagAccounts_TagAccountId",
                table: "Accounts",
                column: "TagAccountId",
                principalTable: "TagAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tags_TagAccounts_TagAccountId",
                table: "Tags",
                column: "TagAccountId",
                principalTable: "TagAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tags_TagTypes_TagTypeId",
                table: "Tags",
                column: "TagTypeId",
                principalTable: "TagTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
