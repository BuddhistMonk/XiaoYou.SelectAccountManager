﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XiaoYou.SelectAccountManager.WebHost.Migrations
{
    public partial class product_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Desc",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PinYin",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Desc",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PinYin",
                table: "Products");
        }
    }
}
