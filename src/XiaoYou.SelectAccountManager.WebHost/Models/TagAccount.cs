﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Models
{
    [Table("TagAccounts")]
    public class TagAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AccountId { get; set; }

        public int TagId { get; set; }

        public virtual Account Account { get; set; }

        public virtual Tag Tag { get; set; }
    }
}
