﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Models
{
    public class SelectAccountManagerContext : DbContext
    {

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<TagType> TagTypes { get; set; }

        public virtual DbSet<Tag> Tags { get; set;}

        public virtual DbSet<Account> Accounts { get; set; }

        public virtual DbSet<TagAccount> TagAccounts { get; set; }

        public SelectAccountManagerContext(DbContextOptions<SelectAccountManagerContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
