﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Models
{


    [Table("Tags")]
    public class Tag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TagTypeId { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public TagType TagType { get; set; }

        public ICollection<TagAccount> TagAccounts { get; set; }

    }
}
