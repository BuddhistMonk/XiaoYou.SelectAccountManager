﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using XiaoYou.SelectAccountManager.WebHost.Dto;
using XiaoYou.SelectAccountManager.WebHost.Models;

namespace XiaoYou.SelectAccountManager.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly SelectAccountManagerContext _context;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductsController(SelectAccountManagerContext context,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            return await _context.Products.ToListAsync();
        }

        [HttpGet("{name}/{pageIndex}")]
        public async Task<ActionResult<ProductIndexDto>> GetProductByName(string name, int pageIndex = 0)
        {
            var productDto = await _context.Products.Where(q => q.Name == name).Select(q => new ProductDto() { Id = q.Id, Name = q.Name }).FirstOrDefaultAsync();
            if (productDto == null)
            {
                return NotFound();
            }
            ProductIndexDto productIndexDto = new ProductIndexDto();

            productIndexDto.TagTypes = await _context.TagTypes.Where(q => q.ProductId == productDto.Id).Select(q => new TagTypeDto() { Id = q.Id, Name = q.Name, ProductId = q.ProductId }).ToListAsync();
            productIndexDto.Tags = await _context.Tags.Where(q => productIndexDto.TagTypes.Select(q => q.Id).Contains(q.TagTypeId)).Select(q => new TagDto() { Id = q.Id, TagTypeId = q.TagTypeId, Name = q.Name, ImageUrl = q.ImageUrl }).ToListAsync();
            //productIndexDto.Accounts = await _context.Accounts.Select(q => new AccountDto() { Id = q.Id, LoginName = q.LoginName }).ToListAsync();
            return productIndexDto;
        }

        [Route("GetProduct")]
        [HttpGet]
        public async Task<ActionResult<ProductIndexDto>> GetProduct()
        {
            var productDto = await _context.Products.Where(q => q.PinYin == "blhx").Select(q => new ProductDto() { Id = q.Id, Name = q.Name }).FirstOrDefaultAsync();
            if (productDto == null)
            {
                return NotFound();
            }
            ProductIndexDto productIndexDto = new ProductIndexDto();

            productIndexDto.TagTypes = await _context.TagTypes.Where(q => q.ProductId == productDto.Id).Select(q => new TagTypeDto() { Id = q.Id, Name = q.Name, ProductId = q.ProductId }).ToListAsync();
            productIndexDto.Tags = await _context.Tags.Where(q => productIndexDto.TagTypes.Select(q => q.Id).Contains(q.TagTypeId)).Select(q => new TagDto() { Id = q.Id, TagTypeId = q.TagTypeId, Name = q.Name, ImageUrl = q.ImageUrl }).ToListAsync();
            //productIndexDto.Accounts = await _context.Accounts.Select(q => new AccountDto() { Id = q.Id, LoginName = q.LoginName }).ToListAsync();
            return productIndexDto;
        }

        [Route("GetAccounts")]
        [HttpGet]
        public async Task<ActionResult<List<AccountDto>>> GetAccounts([FromQuery(Name ="tags[]")]int []tags)
        {
            var accounts = await _context.Accounts.Select(q => new AccountDto() { Id = q.Id, LoginName = q.LoginName, ServeName = q.ServeName, Desc = q.Desc }).ToListAsync();
            return accounts;
        }
    }
}
