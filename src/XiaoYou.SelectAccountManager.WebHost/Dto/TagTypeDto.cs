﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Dto
{
    public class TagTypeDto
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

    }
}
