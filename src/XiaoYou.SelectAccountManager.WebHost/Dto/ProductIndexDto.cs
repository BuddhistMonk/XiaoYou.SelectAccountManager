﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Dto
{
    public class ProductIndexDto
    {
        public ProductDto Product { get; set; }

        public List<TagTypeDto> TagTypes { get; set; }

        public List<TagDto> Tags { get; set; }

        public List<AccountDto> Accounts { get; set; }
    }
}
