﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Dto
{
    public class AccountDto
    {
        public int Id { get; set; }

        public string LoginName { get; set; }

        public string Tags { get; set; }

        public string ServeName { get; set; }

        public string Desc { get; set; }
    }
}
 