﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiaoYou.SelectAccountManager.WebHost.Dto
{
    public class TagDto
    {
        public int TagTypeId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

    }
}
